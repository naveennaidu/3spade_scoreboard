//
//  PlayersCollectionViewProvider.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 26/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import Foundation
import UIKit

protocol PlayersCollectionViewProviderDelegate {
    
    func addPersonClicked()
    func didSelectOn(person : PersonDetails)
    var data: [PersonDetails] {get set}
}

extension PlayersCollectionViewProviderDelegate {
    func didSelectOn(person : PersonDetails) {}
}

class PlayersCollectionViewProvider : NSObject  {
    
    var collectionView : UICollectionView!
    
    var PlayersDelegate: PlayersCollectionViewProviderDelegate!
    
    var data: [PersonDetails] {
        
        return PlayersDelegate.data
    }
    
    init(collectionView: UICollectionView, delegate: PlayersCollectionViewProviderDelegate) {
        super.init()
        self.PlayersDelegate = delegate
        self.collectionView = collectionView
        self.setUpCollectionView()
    }

    private func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        collectionView.reloadData()
    }

    
    var showAddPerson : Bool = true
}

extension PlayersCollectionViewProvider : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private func isAddPlayer(indexPath: IndexPath) -> Bool {
        return showAddPerson && data.count == indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count + (showAddPerson ? 1  : 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        
        if isAddPlayer(indexPath: indexPath) {
            
            cell.populatePerson(person: nil)
                
        } else {
            
            let person = data[indexPath.row]
            
            cell.populatePerson(person: person)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if isAddPlayer(indexPath: indexPath) {
           PlayersDelegate.addPersonClicked()
        } else {
            PlayersDelegate.didSelectOn(person: data[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
}
