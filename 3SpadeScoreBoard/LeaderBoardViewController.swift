//
//  LeaderBoardViewController.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 16/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

protocol LeaderBoardViewControllerDelegate {
    
    func selectedPersonsData(_ : [PersonDetails])
}

class LeaderBoardViewController: UIViewController {
    
    var selectionType : ImageSelection = .forBidWinner
    
    var selectedIndex : Int?
    
    var delegate : LeaderBoardViewControllerDelegate!
    
    @IBOutlet var leaderBoardTableView: UITableView!
    
    var persons : [PersonDetails] = []
    
    var selectedPersons : [PersonDetails] = []
    
    @IBAction func CancelButton(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func AddTeamMates(_ sender: UIBarButtonItem) {
        
        delegate.selectedPersonsData(selectedPersons)
        self.dismiss(animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leaderBoardTableView.dataSource = self
        leaderBoardTableView.delegate = self
        leaderBoardTableView.tableFooterView = UIView()
    }
}

extension LeaderBoardViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return persons.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderBoardTableViewCell") as! LeaderBoardTableViewCell
        
        cell.playerImage.image = persons[indexPath.row].image
        cell.nameLabel.text = persons[indexPath.row].player
        
        if selectionType == .forBidWinner {
           
            if selectedIndex == indexPath.row {
                
                cell.accessoryType = .checkmark
                selectedPersons = []
                selectedPersons.append(persons[selectedIndex!])
                
                
            } else {
                    cell.accessoryType = .none
                
            }
            
        }
        
        return cell
    }
}
extension LeaderBoardViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectionType == .forBidWinner {
            
            selectedIndex = indexPath.row
            
            tableView.reloadData()
            
            
        } else {
            
            if let index = selectedPersons.index(where: {$0.player == persons[indexPath.row].player}) {
                selectedPersons.remove(at: index)
                tableView.cellForRow(at: indexPath)?.accessoryType = .none
            } else {
                selectedPersons.append(persons[indexPath.row])
                tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        selectedPersons = []
    }
}







