//
//  NamedImageView.swift
//  TLLoanApp
//
//  Created by Apple on 26/06/17.
//  Copyright © 2017 Tilicho Labs. All rights reserved.
//

import Foundation
import UIKit

class NamedImageView: UIImageView {
    
    @IBInspectable
    var allowNaming: Bool = true {
        didSet {
            if allowNaming {
                showNameMode()
            }else {
                showNormalMode()
            }
        }
    }
    
    @IBInspectable
    var firstName: String = "" {
        didSet {
            self.setLabelText()
        }
    }
    
    @IBInspectable
    var secondName: String = ""{
        didSet {
            self.setLabelText()
        }
    }
    
    @IBInspectable
    var titleBackgroundColor: UIColor = .lightGray {
        didSet {
            self.backgroundColor = titleBackgroundColor
        }
    }
    
    //Private properties
    private var nameLabel : UILabel = UILabel()
    private var storedImage : UIImage?
    private var originalBackGroundColor : UIColor?
    
    private func showNormalMode() {
        
        nameLabel.isHidden = true
        
        if self.image == nil {
            self.image = storedImage
        }
        
        if self.backgroundColor == titleBackgroundColor {
            self.backgroundColor = originalBackGroundColor
        }
    }
    
    func addNameLabel() {
        
        self.nameLabel.frame = self.bounds
        self.nameLabel.textAlignment = .center
        self.nameLabel.textColor = .white
        self.nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.addSubview(nameLabel)
        
        if allowNaming {
            self.showNameMode()
        } else {
            self.showNormalMode()
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addNameLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addNameLabel()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.nameLabel.frame = self.bounds
    }
    
    private func setLabelText() {
        self.nameLabel.text = (
            firstName == "" ?
                "?" :
                "\(firstName.characters.first!)" +
                (
                    secondName.uppercased().characters.first == nil ?
                        "" : "\(secondName.uppercased().characters.first!)"
            )
        )
    }
    
    private func showNameMode() {
        
        self.nameLabel.isHidden = false
        self.nameLabel.frame = self.bounds
        self.setLabelText()
        self.storedImage = self.image
        self.image = nil
        self.backgroundColor = self.titleBackgroundColor
    }
    
}
