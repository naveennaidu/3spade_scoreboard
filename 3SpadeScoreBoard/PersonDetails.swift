//
//  PersonDetails.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 16/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import Foundation
import UIKit


class PersonDetails: Equatable {
    
    var player : String? 
    var score : Int = 0
    var image : UIImage?
    
}

func ==(lhs: PersonDetails, rhs: PersonDetails) -> Bool {
    return lhs.player == rhs.player
}
