//
//  ImageCollectionViewCell.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 19/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var image: NamedImageView!{
        didSet {
            image.layer.cornerRadius = min(image.bounds.size.width,image.bounds.size.height )/2
            image.layer.masksToBounds = true
            image.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imageNameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    func populatePerson(person: PersonDetails?) {
        
        if person == nil {
            
            imageNameLabel.text = "Add"
            image.image = #imageLiteral(resourceName: "addImage")
            image.allowNaming = false
            scoreLabel.isHidden = true
            
        } else {
            
            imageNameLabel.text = person!.player
            
            if person!.image == nil {
               
                image.firstName = person!.player!.components(separatedBy: " ").first ?? ""
                image.secondName = person!.player!.components(separatedBy: " ").count > 1 ? person!.player!.components(separatedBy: " ")[1] : ""
                
                image.allowNaming = true
                
            } else {
            
                image.allowNaming = false
                image.image = person!.image!
            }
            scoreLabel.isHidden = false
            scoreLabel.text = String(person?.score ?? 0)
        }
    }
    
}
