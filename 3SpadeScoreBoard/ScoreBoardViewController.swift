//
//  ScoreBoardViewController.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 16/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit
import Foundation

enum ImageSelection {
    
    case forBidWinner,forTeamMates,none
}

class ScoreBoardViewController: UIViewController , AddPlayerViewControllerDelegate , PlayersCollectionViewProviderDelegate, LeaderBoardViewControllerDelegate {
    
    lazy var playerCollectionViewProvider : PlayersCollectionViewProvider = {
        
        let p = PlayersCollectionViewProvider(collectionView: self.imageCollectionView, delegate: self)
        return p
        
    } ()
    
    var teamMates : [PersonDetails] = []
    
    fileprivate var bidWinner : PersonDetails? = nil
    
    fileprivate var selectionType = ImageSelection.none
    
    var data : [PersonDetails] = []
    
    var dataToPresent : [PersonDetails] = []
    
    var totalBids : [Bid] = []
    
    fileprivate var presentBid : Bid?
    
    @IBOutlet weak var bidTableView: UITableView!
    
    fileprivate var bidEntryCell: BidTableViewCell!
    
    @IBOutlet var imageCollectionView : UICollectionView!
    
    override func viewDidLoad() {
        
        setupTableView()
        playerCollectionViewProvider.collectionView.reloadData()
    }
    
    private func setupTableView() {
        
        bidTableView.dataSource = self
   
    }
    
    func setUpFooterView() -> UIView {
        
        let footerView = UIView(frame : CGRect(x: 0, y: 0, width: self.bidTableView.bounds.size.width , height: 50))
        
        footerView.backgroundColor = UIColor.darkGray
        
        let submitButton = UIButton(frame : CGRect(x: self.bidTableView.bounds.size.width/2 - 60, y: 5, width:120, height: 40))
        
        submitButton.backgroundColor = UIColor.white
        
        submitButton.setTitle("Submit", for: .normal)
        
        submitButton.setTitleColor(UIColor.black, for: .normal)
        
        submitButton.addTarget(self, action: #selector(createBid), for: .touchUpInside)
        
        footerView.addSubview(submitButton)
        
        return footerView
    }
    
    func createBid() {
        
        createBidDetails(cell: bidEntryCell)
        imageCollectionView.reloadData()
    }
    
    func addPlayer(_ newPlayer : PersonDetails) {
        
        data.append(newPlayer)
        imageCollectionView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        bidTableView.tableFooterView = setUpFooterView()
    }
    
    func imgselection(selection : ImageSelection) {
        
        switch selection {
            
        case  .forBidWinner :
            
            selectionType = selection
            
            let playersListNavigationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
                "playersListNavigationVC") as! UINavigationController
            
            let playersListVC = playersListNavigationVC.topViewController as! LeaderBoardViewController
            
            playersListVC.leaderBoardTableView?.allowsMultipleSelection = false
            
            playersListVC.delegate = self
            
            playersListVC.persons = data
            playersListVC.selectionType = .forBidWinner
            
            self.present(playersListNavigationVC, animated: true, completion: nil)
            
        case .forTeamMates :
            
            selectionType = selection
            
            let playersListNavigationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
                "playersListNavigationVC") as! UINavigationController
            
            let playersListVC = playersListNavigationVC.topViewController as! LeaderBoardViewController
            
            playersListVC.delegate = self
            
            playersListVC.leaderBoardTableView?.allowsMultipleSelection = true
            
            playersListVC.selectionType = .forTeamMates
            
            dataToPresent = data
            
            if bidWinner != nil{
                
            dataToPresent.remove(at: dataToPresent.index(of: bidWinner!)!)
            }

            playersListVC.persons = (bidWinner != nil) ? dataToPresent : data
            
            self.present(playersListNavigationVC, animated: true, completion: nil)
            
        default:
            break
        }
        
    }
    
    func selectedPersonsData(_ selectedPlayers : [PersonDetails]) {
        
        for players in selectedPlayers {
            
            if selectionType == ImageSelection.forBidWinner {
                
            bidWinner = players
            }
            if selectionType == ImageSelection.forTeamMates {
                teamMates.append(players)
            }
            bidTableView.reloadData()
        }
        
    }
    
    func assignScores(presentBid : Bid) {
        
        for player in data {
            
            if presentBid.bidStatus {
                
                if player.player == presentBid.bidWinner {
                    
                    player.score = 2 * presentBid.bidScore
                }
                player.score = teamMates.contains(player) ? presentBid.bidScore : 0
                
            } else {
                
                if player.player == presentBid.bidWinner {
                    
                    player.score = presentBid.bidScore
                }
                player.score = teamMates.contains(player) ? 0 : presentBid.bidScore
            }
        }
        
    }
    
    func addPersonClicked() {
        
        let navigationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
            "AddPlayerNavigationViewController") as! UINavigationController
        
        let addPlayerVC = navigationVC.topViewController as! AddPlayerViewController
        addPlayerVC.delegate = self
        
        self.present(navigationVC, animated: true, completion: nil)
    }
}

extension ScoreBoardViewController: UITableViewDataSource ,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 2
    }
    
    public func tableView(_ tableview: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row == 0 {
            
            let cell = tableview.dequeueReusableCell(withIdentifier: "BidTableViewCell") as! BidTableViewCell
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            cell.bidWinnerImg.isUserInteractionEnabled = true
            cell.bidWinnerImg.addGestureRecognizer(tapGestureRecognizer)
            
            if bidWinner != nil {
                
                cell.bidWinnerImg.image = bidWinner?.image
                cell.bidWinnerNameLabel.text = bidWinner?.player
            }
            
            self.bidEntryCell = cell
            
            return cell
        }
        else {
            
            let cell = tableview.dequeueReusableCell(withIdentifier: "TeamMatesCollectionTableViewCell") as! TeamMatesCollectionTableViewCell
            cell.parentDelegate = self
            cell.data = self.teamMates
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame : CGRect(x: 0, y: 0, width: self.bidTableView.bounds.size.width , height: 50))
        
        headerView.backgroundColor = UIColor.darkGray
        
        let leftButton = UIButton(frame : CGRect(x: self.bidTableView.bounds.size.width/2 - 60, y: 5, width:120, height: 40))
        
        let rightButton = UIButton(frame : CGRect(x: self.bidTableView.bounds.size.width/2 - 60, y: 5, width:120, height: 40))
        
        rightButton.setTitle("+", for: .normal)
        leftButton.setTitle("<", for: .normal)
        
        leftButton.isHidden = true
        
        leftButton.addTarget(self, action: #selector(leftButtonAction), for: .touchUpInside)
        
        rightButton.addTarget(self, action: #selector(rightButtonAction), for: .touchUpInside)
        
        headerView.addSubview(leftButton)
        headerView.addSubview(rightButton)
        
        return headerView
    }
    
    func leftButtonAction() {
        
        
    }
    
    func rightButtonAction() {
       

    }
    
    func imageTapped(tapGestureRecognizer : UITapGestureRecognizer) {
        

     
        imgselection(selection: .forBidWinner)
    }
    
    func createBidDetails(cell : BidTableViewCell){
        
        if (bidEntryCell != nil) {
            
            presentBid = Bid()

            
            presentBid?.bidWinner = bidEntryCell.bidWinnerNameLabel.text
            
            if !bidEntryCell.bidWinnerTextField.isEmpty() {
             
                presentBid?.bidScore = Int(bidEntryCell.bidWinnerTextField.text!)!
                
            } else {
                
                let alert = UIAlertController(title: "Alert", message: "bid score should not be empty", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
               
                print("bklhsfjsdhfv")
            }
            
            presentBid?.bidStatus = bidEntryCell.bidStatusButton.isOn
            presentBid?.teamMates = teamMates
            presentBid?.roundNo = (presentBid?.roundNo)! + 1
            
            print ("round no :   \(String(describing: presentBid?.roundNo)) ")
            totalBids.append(presentBid!)
            
            assignScores(presentBid: presentBid! )
    }
}
}
