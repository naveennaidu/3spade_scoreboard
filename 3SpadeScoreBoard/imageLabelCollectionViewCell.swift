//
//  imageLabelCollectionViewCell.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 23/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

class imageLabelCollectionViewCell: UICollectionViewCell {
    
    var textLabel : UILabel!
    var imageview : UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height * 2/3))
        imageview.contentMode = .scaleAspectFill
        contentView.addSubview(imageview)
        
        textLabel = UILabel(frame: CGRect(x: 0, y: imageview.frame.size.height, width: frame.size.width, height: frame.size.height/3))
        textLabel.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        textLabel.textAlignment = .center
        contentView.addSubview(textLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
