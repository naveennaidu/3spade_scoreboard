//
//  BidTableViewCell.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 16/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

class BidTableViewCell: UITableViewCell {

    @IBOutlet var bidWinnerImage: UIImageView!
    
    @IBOutlet var scoreLabel: UILabel!
    
    @IBOutlet var scoreTextField: UITextField!
}
