//
//  AddPlayerViewController.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 20/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

protocol AddPlayerViewControllerDelegate {
    
    func  addPlayer(_ : PersonDetails)
}

class AddPlayerViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    var delegate : AddPlayerViewControllerDelegate!
    
    var newPerson = PersonDetails()
    
    @IBOutlet var playerImage: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBAction func createPlayerbutton(_ sender: Any) {
        
        newPerson = PersonDetails()
        createPlayer()
        self.dismiss(animated: true, completion: {
            
            self.delegate.addPlayer(self.newPerson)
        })
    }
    
    @IBAction func createAddButton(_ sender: Any) {
        
        newPerson = PersonDetails()
        
        createPlayer()
        self.delegate.addPlayer(self.newPerson)
        resetFields()
    }
    
    @IBAction func dismissButton(_ sender : UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add Player"
        
        playerImage.layer.cornerRadius = min(playerImage.layer.bounds.size.width,playerImage.layer.bounds.size.height)/2
        playerImage.layer.masksToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        playerImage.isUserInteractionEnabled = true
        playerImage.addGestureRecognizer(tapGestureRecognizer)
        self.hideKeyboardWhenTappedAround()
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        playerImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func  createPlayer()  {
        
        newPerson.player = nameTextField.text
        newPerson.image = playerImage.image
    }
    
    func resetFields()  {
        
        nameTextField.text = nil
        playerImage.image = nil
    }
    
}
extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

