 //
//  TeamMatesCollectionTableViewCell.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 26/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

class TeamMatesCollectionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var teamMatesCollectionView: UICollectionView!
    
    var parentDelegate : ScoreBoardViewController!
    
    var data : [PersonDetails] = [] {
        didSet {
            awakeFromNib()
        }
    }
 
    lazy var playerCollectionViewProvider : PlayersCollectionViewProvider = {
        
        let p = PlayersCollectionViewProvider(collectionView: self.teamMatesCollectionView, delegate: self)
        return p
     } ()
    
    override func awakeFromNib() {
            
        playerCollectionViewProvider.collectionView.reloadData()
    }
}

extension TeamMatesCollectionTableViewCell : PlayersCollectionViewProviderDelegate, LeaderBoardViewControllerDelegate {
    
    func addPersonClicked() {
        
        parentDelegate.imgselection(selection: .forTeamMates)
      }
    
    func selectedPersonsData(_ mates: [PersonDetails]) {
        
        self.data = mates
        self.teamMatesCollectionView.reloadData()
    }
 }
