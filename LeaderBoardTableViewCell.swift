//
//  LeaderBoardTableViewCell.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 21/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit

class LeaderBoardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var playerImage : UIImageView! {
        didSet {
            playerImage.layer.cornerRadius = min(playerImage.bounds.size.width,playerImage.bounds.size.height)/2
            playerImage.layer.masksToBounds = true
            playerImage.clipsToBounds = true
        }
    }
}
