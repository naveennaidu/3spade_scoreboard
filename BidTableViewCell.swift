//
//  BidTableViewCell.swift
//  3SpadeScoreBoard
//
//  Created by Kattappa on 16/06/17.
//  Copyright © 2017 Tilicho. All rights reserved.
//

import UIKit


class BidTableViewCell: UITableViewCell {

    @IBOutlet weak var bidWinnerTextField: UITextField! {
        
        didSet{
            
            bidWinnerTextField.keyboardType = .numberPad
        }
    }
    @IBOutlet weak var bidWinnerNameLabel: UILabel!
    @IBOutlet weak var bidWinnerImg: UIImageView! {
        didSet {
            
            bidWinnerImg.layer.cornerRadius = min(bidWinnerImg.bounds.size.width,bidWinnerImg.bounds.size.height )/2
            bidWinnerImg.layer.masksToBounds = true
            bidWinnerImg.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var bidStatusButton: UISwitch!
}

